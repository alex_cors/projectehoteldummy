package net.krespy.kotel.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;
import net.krespy.kotel.R;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Carlos on 24/02/2015.
 * RecyclerView permite hacer ListViews más óptimas y con card views (Android L).
 * RecyclerView actua de adapter o controller entre el dataset y las vistas.
 * El dataset lo cargaremos en un array, y la vista la enviaremos a un ViewHolder.
 */

/** Cada itemLayout es una Row de una lista Recyclerview. Cada row será una cardview (bt_card_row).
 * BT_Devices es el dataset que contendrá todos los dispositivos encontrados.*/
public class BeaconsListAdapter extends RecyclerView.Adapter<BeaconsListAdapter.ViewHolder>{
    private ArrayList<Beacon> beacons;
    private LayoutInflater inflater;

    public BeaconsListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.beacons = new ArrayList<Beacon>();
    }

    public void replaceWith(Collection<Beacon> newBeacons) {
        this.beacons.clear();
        this.beacons.addAll(newBeacons);
        notifyDataSetChanged();
    }

    /** Vamos a modificar dinámicamente una vista o Layout con el método ifnlate. Cargaremos los
     *  itemLayout (Las rows con cada BT_Device detectado) a la vista o fragmento que queremos.*/
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bt_card_row,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Beacon beacon = beacons.get(position);
        holder.macTextView.setText(String.format("MAC: %s (%.2fm)", beacon.getMacAddress(), Utils.computeAccuracy(beacon)));
        holder.majorTextView.setText("Major: " + beacon.getMajor());
        holder.minorTextView.setText("Minor: " + beacon.getMinor());
        holder.measuredPowerTextView.setText("MPower: " + beacon.getMeasuredPower());
        holder.rssiTextView.setText("RSSI: " + beacon.getRssi());
    }

    @Override
    public int getItemCount() {
        return beacons.size();
    }

    /** ViewHolder evita hacer muchas veces un findViewById, ya que esto sobrecarga mucho la aplicación
     * y ralentizaría el scroll. De esta manera lo carga una vez y lo guarda en un viewHolder.
     * A partir de Android L es obligatorio substituir LisytView por Recycler y usar ViewHolders.**/
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView macTextView;
        private TextView majorTextView;
        private TextView minorTextView;
        private TextView measuredPowerTextView;
        private TextView rssiTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            macTextView = (TextView) itemView.findViewById(R.id.txtMAC);
            majorTextView =(TextView) itemView.findViewById(R.id.txtMajor);
            minorTextView = (TextView) itemView.findViewById(R.id.txtMinor);
            measuredPowerTextView = (TextView) itemView.findViewById(R.id.txtPower);
            rssiTextView = (TextView) itemView.findViewById(R.id.txtRessi);
        }
    }
}

